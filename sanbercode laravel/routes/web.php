<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/', [HomeController::class,'index']);

Route::get('/register', [AuthController::class,'register']);
Route::get('/home', [AuthController::class,'welcome']);

Route::get('/table', function(){
    return view('halaman.table');
});
Route::get('/data-table', function(){
    return view('halaman.data-table');
});

// CRUD CAST SANBER

// CAST
// FORM TAMBAH CAST
// CREATE
Route::get('/cast/create',[CastController::class,'create']);
// route kirim data/tmbh data ke db
Route::post('/cast',[CastController::class,'store']);

// READ
Route::get('/cast',[CastController::class,'index']);
// detail data berdasarkan id params
Route::get('/cast/{id}',[CastController::class,'show']);

// UPDATE
Route::get('/cast/{id}/edit',[CastController::class,'edit']);
// update data berdsarkan id
Route::put('/cast/{id}',[CastController::class,'update']);

// DELETE
Route::delete('/cast/{id}',[CastController::class,'destroy']);