@extends('layout.master')

@section('judul')
    Halaman Edit
@endsection

@section('content')
    <form action="/cast/{{ $cast->id }}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label>nama</label>
            <input type="text" class="form-control" value="{{ $cast->nama }}" id="nama" name="nama" placeholder="nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>umur</label>
            <input type="integer" name="umur" value="{{ $cast->umur }}" class="form-control" id="umur" placeholder="umur">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>bio</label>
            <textarea type="text" name="bio" class="form-control" id="bio" placeholder="bio" cols="30" rows="10">{{ $cast->bio }}</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        
        <button type="submit" class="btn btn-warning">update</button>
    </form>
@endsection
