@extends('layout.master')

@section('judul')
    Halaman Tampil Cast
@endsection

@section('content')
    <a href="/cast/create" class="btn btn-primary my-3 btn-sm">tmbh</a>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                @forelse ($cast as $index => $casts)
                    <th scope="row">{{ $index + 1 }}</th>
                    <td>{{ $casts->nama }}</td>
                    <td>
                        <a href="/cast/{{ $casts->id }}" class="btn btn-info btn-sm" >detail</a>
                        <a href="/cast/{{ $casts->id }}/edit" class="btn btn-warning btn-sm">edit</a>
                        <form action="/cast/{{ $casts->id }}" method="POST">
                            @method('delete')
                            @csrf
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
            </tr>
            @empty
            <tr>
            <td>tidak ada data cast</td>
            </tr>
            @endforelse
        </tbody>
    </table>
@endsection
