@extends('layout.master')

@section('judul')
    halaman biodata
@endsection

@section('content')
    
<h1>Buat Account Baru!</h1>
<h2>Sign Up Form</h2>
<form action="/home">
    <label>First name:</label><br><br>
    <input type="text" name="firstName"><br><br>
    <label>Last name:</label><br><br>
    <input type="text" name="lastName"><br><br>

    <label>Gender:</label><br><br>
    <input type="radio">Male<br>
    <input type="radio">Female<br>
    <input type="radio">Other<br><br>

    <label>Nationality:</label><br><br>
    <select name="negara">
        <option value="Indonesian">Indonesian</option>
        <option value="English">English</option>
        <option value="Other">Other</option>
    </select><br><br>

    <label>Language Spoken:</label><br><br>
    <input type="checkbox" name="Language Spoken">Bahasa Indonesia<br>
    <input type="checkbox" name="Language Spoken">English<br>
    <input type="checkbox" name="Language Spoken">Other<br><br>

    <label>Bio:</label><br><br>
    <textarea name="bio" rows="10" cols="30"></textarea><br><br>
    <input type="submit" value="Sign Up">
</form>
@endsection
