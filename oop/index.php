<?php
require_once('animal.php');
require_once('buduk.php');
require_once('kera.php');

$animal = new Animal("shaun");

echo "nama: " . $animal->nama . "<br>";
echo "legs: " . $animal->legs . "<br>";
echo "cold blooded: " . $animal->coldBlodeed . "<br> <br>";

$buduk = new buduk("buduk");

echo "nama: " . $buduk->nama . "<br>";
echo "legs: " . $buduk->legs . "<br>";
echo "cold blooded: " . $buduk->coldBlodeed . "<br>";
echo $buduk->jump() . "<br> <br>";

$kera = new kera("kera sakti");

echo "nama: " . $kera->nama . "<br>";
echo "legs: " . $kera->legs . "<br>";
echo "cold blooded: " . $kera->coldBlodeed . "<br>";
echo $kera->yell();

?>